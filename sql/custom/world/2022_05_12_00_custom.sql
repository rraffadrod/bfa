DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=25 AND `SourceEntry`=1190;
INSERT INTO `conditions` VALUES
(25, 0, 1190, 0, 0, 27, 0, 90, 3, 0, 0, 0, 0, '', 'TerrainSwap 1190 only when player has level 90'),
(25, 0, 1190, 0, 0, 28, 0, 34398, 0, 0, 0, 0, 0, '', 'Terrainwap: Warlords of Draenor: The Dark Portal when rewarded quest 34398'),
(25, 0, 1190, 0, 0, 8, 0, 34398, 0, 0, 0, 0, 0, '', 'Terrainwap: Warlords of Draenor: The Dark Portal when complete quest 34398');
